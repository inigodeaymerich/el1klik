<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
            // Nombre de la tabla en MySQL
            public $table = "opiniones";

            // Colunma 1 de la tabla, clave primaria y auto-increment
            public $primaryKey = "id_opinion";
        
            // Columnas de la tabla (normales)
            public $fillable = ["id_opinion", "titulo_opinion", "texto_opinion", "valoracion", "fecha_publicacion", "Publicada",
            "codigo_ciclo", "codigo_centro","email"];
        
            // Columnas con datos ocultos
            public $hidden = ['created_at','updated_at'];           
            
            //FK a ciclo
            public function ciclo() {
            return $this->belongsTo('App\Ciclos', 'codigo_ciclo');
            }            
            
            //FK a centro
            public function centro() {
            return $this->belongsTo('App\centro', 'codigo_centro');
            }
            
            //FK a centro
            public function usuario() {
            return $this->belongsTo('App\User', 'email', 'email');
            }            
        
            // Relación 
            public function centros() {  
            return $this -> hasMany("App\Opinion");          
            }
}
