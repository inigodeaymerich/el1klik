<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Familias extends Model
{
    // Nombre de la tabla en MySQL
    public $table = "familias";

    // Colunma 1 de la tabla, clave primaria y auto-increment
    public $primaryKey = "codigo_familia";

    // Columnas de la tabla (normales)
    public $fillable = ["codigo_familia", "familia_ciclo_es", "familia_ciclo_en", "familia_ciclo_eus", "descripcion", "imagen_familia", "familia_destacadas"];

    // Columnas con datos ocultos
    public $hidden = ['created_at','updated_at'];

    // Relación 
    public function familias() {  
    return $this -> hasMany("App\Opinion");          
    }
    // Relación 
    public function ciclos() {  
    return $this -> hasMany("App\Ciclos");          
    }
}
