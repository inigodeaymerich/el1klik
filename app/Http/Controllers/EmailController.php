<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail; //Importante incluir la clase Mail, que será la encargada del envío

class EmailController extends Controller
{
  
    public function contacto(Request $request){
        $subject = "Asunto del correo";
        $for = "ekain.pecina@ikasle.aeg.es";
        Mail::send('email',$request->all(), function($msj) use($subject,$for){
            $msj->from("tucorreo@gmail.com","1klik Administrador");
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect()->back()->with('success', 'Su mensaje se ha enviado correctamente, recibiras su respuesta lo antes posible. Muchas gracias.');
    }
}