<?php

namespace App\Http\Controllers;
use App\Opinion;
use App\centro;
use App\Ciclos;
use App\Familias;
use App\User;
use App\Tienen;
use Carbon\Carbon;

use Illuminate\Http\Request;

class OpinionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $opiniones = Opinion::all(); 
       return view("opiniones.create", compact("opiniones"));      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centros=centro::all();
        $ciclos=Ciclos::all();
      //  $prueba=Ciclos::with('centros')->first();
       
    
        return view('opiniones.create', compact('centros','ciclos'));
    }

   /* public function detail($nombre_centro)
    {
        $centroSelec = Centro::where('nombre_centro','=', $nombre_centro)->first();
        return view("centro", compact("centroSelec"));
        
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['titulo_opinion'=>'required', 'texto_opinion'=>'required', 'valoracion'=>'required']);
        
        Opinion::create($request->all());
        return redirect()->route('opiniones.create')->with('success', 'Su opinión va a ser valorada, Muchas gracias por su colaboración');
    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
   /* public function show($codigo_centro)
    {
        $familias=Centro::find($codigo_centro);
        return  view('centro.show',compact('centro'));
    }*/
    public function show()
    {   
        
        $opiniones=Opinion::all();
        return  view('opiniones.show',compact('opiniones')); 
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $codigo_familia
     * @return \Illuminate\Http\Response
     */
    public function edit($codigo_familia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $codigo_familia
     * @return \Illuminate\Http\Response
     */
    public function update($id_opinion)
    {
        $Opiniones = Opinion::find($id_opinion);
        $Opiniones->fecha_publicacion=Carbon::now();
        $Opiniones->publicada = 1;
        $Opiniones->save();
        return back()->with('success', '¡La opinión se ha validado correctamente!');
        
    }
    


    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $codigo_familia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_opinion)
    {
      
        Opinion::find($id_opinion)->delete();
        return redirect()->route('admin.centro_admin')->with('success','Registro eliminado satisfactoriamente');
       
    
    }
}
