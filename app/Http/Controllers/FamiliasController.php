<?php

namespace App\Http\Controllers;
use App\Ciclos;
use App\Familias;


use Illuminate\Http\Request;
use Illuminate\Database\Seeder;


class FamiliasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $familias = Familias::all(); 
       return view("familia.index", compact("familias"));      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('familia.create');
    }

   /* public function detail($nombre_centro)
    {
        $centroSelec = Centro::where('nombre_centro','=', $nombre_centro)->first();
        return view("centro", compact("centroSelec"));
        
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['codigo_familia'=>'required', 'familia_ciclo_es'=>'required', 'familia_ciclo_en'=>'required', 'familia_ciclo_eus'=>'required', 'descripcion'=>'required', 'imagen_familia'=>'required', 'familia_destacadas'=>'nullable']);
        //$request->codigo_familia=str_pad($codigo_familia,8,"0",STR_PAD_LEFT);
        Familias::create($request->all());
        return redirect()->route('familia.index')->with('success','Registro creado satisfactoriamente');
    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
   /* public function show($codigo_centro)
    {
        $familias=Centro::find($codigo_centro);
        return  view('centro.show',compact('centro'));
    }*/
    public function show($codigo_familia)
    {   
        $codigo_familia=str_pad($codigo_familia,2,"0",STR_PAD_LEFT);
        $familia=Familias::find($codigo_familia);
        $ciclos=Ciclos::all();
        return  view('familia.show',compact('familia','ciclos','codigo_familia')); 
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $codigo_familia
     * @return \Illuminate\Http\Response
     */
    public function edit($codigo_familia)
    {
        $codigo_familia=str_pad($codigo_familia,2,"0",STR_PAD_LEFT);
        $familias=Familias::find($codigo_familia);

        return view('familia.edit',compact('familia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $codigo_familia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $codigo_familia)
    {
       $this->validate($request,['codigo_familia'=>'required', 'familia_ciclo_es'=>'required', 'familia_ciclo_en'=>'required', 'familia_ciclo_eus'=>'required', 'descripcion'=>'required', 'imagen_familia'=>'required', 'familia_destacadas'=>'nullable']);
 
       Familias::find($codigo_familia)->update($request->all());
        return redirect()->route('familia.index')->with('success','Registro actualizado satisfactoriamente');
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $codigo_familia
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo_familia)
    {
        $codigo_familia=str_pad($codigo_familia,2,"0",STR_PAD_LEFT);
        Familias::find($codigo_familia)->delete();
        return redirect()->route('admin.centro_admin')->with('success','Registro eliminado satisfactoriamente');
    }
}
 