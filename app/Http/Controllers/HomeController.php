<?php

namespace App\Http\Controllers;
use App\Opinion;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
    
    }
    public function show()
    {
        $opiniones = Opinion::all();
        $opinionesrecientes = Opinion::orderBy('fecha_publicacion', 'DESC')->where('publicada','1')->get()->take(4);
        
        return view('index', compact('opinionesrecientes','opiniones'));
    }
}
