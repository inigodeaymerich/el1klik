<?php

namespace App\Http\Controllers;
use App\centro;
use App\Opinion;

use Illuminate\Http\Request;
use Illuminate\Database\Seeder;


class centroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $centros = Centro::all(); 
       $centros = Centro::orderBy('nombre_centro','ASC')->get();
       return view("centro.index", compact("centros"));      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('centro.create');
    }

   /* public function detail($nombre_centro)
    {
        $centroSelec = Centro::where('nombre_centro','=', $nombre_centro)->first();
        return view("centro", compact("centroSelec"));
        
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['codigo_centro'=>'required', 'nombre_centro'=>'required', 'ciudad'=>'required', 'direccion'=>'required', 'telefono'=>'required', 'web'=>'required']);
       
        //$request->codigo_centro=str_pad($codigo_centro,8,"0",STR_PAD_LEFT);
        Centro::create($request->all());
        
        return redirect()->route('admin.centro_admin')->with('success','Registro creado satisfactoriamente');
    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
   /* public function show($codigo_centro)
    {
        $centro=Centro::find($codigo_centro);
        return  view('centro.show',compact('centro'));
    }*/
  public function show($codigo_centro)
    {   
        $codigo_centro=str_pad($codigo_centro,6,"0",STR_PAD_LEFT);
        $centrodet=Centro::find($codigo_centro);
        
        $centros = Centro::all(); 
        $centros = Centro::orderBy('nombre_centro','ASC')->get();
        $opinionesrecientes = Opinion::orderBy('fecha_publicacion', 'DESC')->where('codigo_centro', $codigo_centro)->where('publicada','1')->get()->take(4);
        
        $media = $opinionesrecientes->count();
        $suma=0;
        if($media>0){
       foreach($opinionesrecientes as $o)
       {
           $o->valoracion;
           $suma+=$o->valoracion;
       }
       $valoracionmedia=round($suma/$media);
    }
       
        
        

        return  view('centro.show',compact('centros','centrodet','opinionesrecientes','valoracionmedia','media'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
    public function edit($codigo_centro)
    {
        $codigo_centro=str_pad($codigo_centro,6,"0",STR_PAD_LEFT);
        $centro=Centro::find($codigo_centro);
        return view('centro.edit',compact('centro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $codigo_centro)
    {
       $this->validate($request,[ 'codigo_centro'=>'required', 'nombre_centro'=>'required', 'ciudad'=>'required', 'direccion'=>'required', 'telefono'=>'required', 'web'=>'required']);
 
        Centro::find($codigo_centro)->update($request->all());
        return redirect()->route('centro.index')->with('success','Registro actualizado satisfactoriamente');
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo_centro)
    {
        $codigo_centro=str_pad($codigo_centro,6,"0",STR_PAD_LEFT);
        Centro::find($codigo_centro)->delete();
        return redirect()->route('admin.centro_admin')->with('success','Registro eliminado satisfactoriamente');
    }
}
 