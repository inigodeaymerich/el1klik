<?php

namespace App\Http\Controllers;
use App\Ciclos;
use App\Centros;
use App\Tienen;
use App\Familias;
use App\Opinion;
use Illuminate\Http\Request;
use Illuminate\Database\Seeder;

class CicloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ciclos = Ciclos::all(); 
        return view("ciclo.index", compact("ciclos"));
    }

    /*public function index()
    {
        $ciclos = Ciclos::find($familia->codigo_familia);
        $familias = Familias::all(); 

        return view("ciclo.index", compact("ciclos","familias"));
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ciclo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['codigo_ciclo'=>'required', 'nombre_ciclo_es'=>'required', 'nombre_ciclo_en'=>'required', 'nombre_ciclo_eus'=>'required', 
        'nivel_ciclo_es'=>'required', 'nivel_ciclo_en'=>'required','nivel_ciclo_eus'=>'required','descripcion_ciclo_es'=>'required',
        'descripcion_ciclo_en'=>'required','descripcion_ciclo_eus'=>'required','codigo_familia'=>'required']);
       
        Ciclos::create($request->all());
        return redirect()->route('ciclo.index')->with('success','Ciclo creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
    public function show($codigo_ciclo)
    {
        $codigo_ciclo=str_pad($codigo_ciclo,5,"0",STR_PAD_LEFT);
        $ciclodet=Ciclos::find($codigo_ciclo);
        //$familia=Familias::find
        $ciclos=Ciclos::all();
        $opinionesrecientes = Opinion::orderBy('fecha_publicacion', 'DESC')->where('codigo_ciclo', $codigo_ciclo)->where('publicada','1')->get()->take(4);
        
        $media = $opinionesrecientes->count();
        $suma=0;
        if($media>0){
       foreach($opinionesrecientes as $o)
       {
           $o->valoracion;
           $suma+=$o->valoracion;
       }
       $valoracionmedia=round($suma/$media);
       //dd($valoracionmedia);
    }
        return  view('ciclo.show',compact('ciclos','ciclodet','codigo_ciclo','opinionesrecientes','valoracionmedia','media')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
    public function edit($codigo_centro)
    {
        $codigo_ciclo=str_pad($codigo_ciclo,5,"0",STR_PAD_LEFT);
        $ciclos=Ciclos::find($codigo_ciclo); 
              
        return  view('ciclo.edit',compact('ciclos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $codigo_centro)
    {
        $this->valcodigo_centroate($request,['codigo_ciclo'=>'required', 'nombre_ciclo_es'=>'required', 'nombre_ciclo_en'=>'required', 'nombre_ciclo_eus'=>'required', 
        'nivel_ciclo_es'=>'required', 'nivel_ciclo_en'=>'required','nivel_ciclo_eus'=>'required','descripcion_ciclo_es'=>'required',
        'descripcion_ciclo_en'=>'required','descripcion_ciclo_eus'=>'required','codigo_familia'=>'required']);
 
        Ciclos::find($codigo_ciclo)->update($request->all());
         return redirect()->route('ciclo.index')->with('success','Ciclo actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $codigo_centro
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo_ciclo)
    {
        $codigo_ciclo=str_pad($codigo_ciclo,5,"0",STR_PAD_LEFT);
        Ciclos::find($codigo_ciclo)->delete();
        return redirect()->route('admin.centro_admin')->with('success','Ciclo eliminado satisfactoriamente');
    }
    public function ajax(Request $request)
    {
        //dd($request->codigo_centro);
        
        $ciclos = Tienen::select('centro_ciclos.ciclos_codigo_ciclo', 'ciclos.nombre_ciclo_es')
        ->join('ciclos', 'ciclos.codigo_ciclo', '=', 'centro_ciclos.ciclos_codigo_ciclo')
        ->where('centros_codigo_centro', $request->codigo_centro)
        ->orderBy('nombre_ciclo_es', 'ASC')
        ->get();
        
        $ciclosjson = json_encode($ciclos);
        dd($ciclosjson);
        
        
        return response()->json($ciclos);

        
    }
}
