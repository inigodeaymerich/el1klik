<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciclos extends Model
{
        // Nombre de la tabla en MySQL
        public $table = "ciclos";

        // Colunma 1 de la tabla, clave primaria y auto-increment
        public $primaryKey = "codigo_ciclo";
    
        // Columnas de la tabla (normales)
        public $fillable = ["codigo_ciclo", "nombre_ciclo_es", "nombre_ciclo_en", "nombre_ciclo_eus", 
        "nivel_ciclo_es", "nivel_ciclo_en", "nivel_ciclo_eus", "descripcion_ciclo_es", "descripcion_ciclo_en", "descripcion_ciclo_eus",
        "codigo_familia"];
    
        // Columnas con datos ocultos
        public $hidden = ['created_at','updated_at'];
    
        // Relación 
        public function opinion() {  
        return $this -> hasMany("App\Opinion");          
        }

        //FK a familia
        public function familia() {
        return $this->belongsTo('App\Familias', 'codigo_familia');
        }

        public function centros() {
        return $this->belongsToMany('App\centro');
        }
}
