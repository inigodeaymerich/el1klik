<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tienen extends Model
{
            // Nombre de la tabla en MySQL
            public $table = "centro_ciclos";

            // Columnas de la tabla (normales)
            public $fillable = ["ciclos_codigo_ciclo", "centros_codigo_centro"];

            //FK a ciclo
            public function ciclo() {
            return $this->belongsTo('App\Ciclos', 'codigo_ciclo');
            }            
            
            //FK a centro
            public function centro() {
            return $this->belongsTo('App\centro', 'codigo_centro');
            }
            // Columnas con datos ocultos
            public $hidden = ['created_at','updated_at'];
        
            // Relación 
            public function centros() {  
            return $this -> hasMany("App\centro");          
            }

            // Relación 
            public function ciclos() {  
            return $this -> hasMany("App\Ciclos"); 
            }              
    
}
