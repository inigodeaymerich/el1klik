<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Centro extends Model
{
        // Nombre de la tabla en MySQL
        public $table = "centros";

        // Colunma 1 de la tabla, clave primaria y auto-increment
        public $primaryKey = "codigo_centro";
    
        // Columnas de la tabla (normales)
        public $fillable = ["codigo_centro", "nombre_centro", "ciudad", "direccion", "telefono", "web"];
    
        // Columnas con datos ocultos
        public $hidden = ['created_at','updated_at'];
    
        // Relación 
        public function centros() {  
        return $this -> hasMany("App\Opinion");          
        }

        //Relacion N:N
        public function ciclos() {
        return $this->belongsToMany('App\Ciclos');
        }
}
